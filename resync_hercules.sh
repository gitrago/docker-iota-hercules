#!/bin/bash
# Written by muXxer

printf "\nStopping Hercules...\n"
docker-compose down
./download_latest_hercules_snapshot.sh
printf "Deleting old Hercules database...\n"
sudo rm -Rf volumes/hercules/data
printf "Restarting Hercules...\n"
docker-compose up -d